# README #

### Object Tracking ###

Object tracking is a method of finding an object in a single frame from a video source and constantly follows it as it moves. 
Video tracking is a key component in everyday technology like security and traffic systems ("Video Tracking Definition", n.d.). 
There are different algorithms for video tracking. This research focuses on 2 algorithms, Mean shift and CAM shift.

### Detail Information ###

* Please let you open a file named objectTracking.doc


